import numpy as np
np.seterr(divide = 'ignore')

def logprior(r, clusterDistance, clusterScaleRadius, membershipProbability, exponentialVolumeCutoff):
    '''log prior probability distribution for stars

       Returns the log of the probability density function (pdf) of the prior
       for a given distance r.

       Arguments:
       r -- distance at which to compute the prior pdf
       clusterDistance -- distance from the observer to the cluster based
           on prior knowledge, e.g. literature data. Should be > 0.
       clusterScaleRadius -- size of the cluster, e.g. its half-mass radius,
           from previous knowledge such as literature data. Should be > 0.
       membershipProbability -- the probability of membership of a star
           estimated independently of parallax, e.g. with proper motions.
           Being a probability, it should be between 0 and 1, inclusive.
       exponentialVolumeCutoff -- length scale in the exponentially decreasing
           volume density prior for field stars by Bailer-Jones (2015).
           Should be > 0.

       Notes:
       The units used for the inputs should be consistent (e.g. all in kpc).
       This function is usually called with the argument r being an array of
       distances onto which the prior pdf needs to be evaluated; this can
       range e.g. from -1 to 40 (units in kpc); clusterDistance is typically
       ~ 1 kpc (as was the case for M67) or less; clusterScaleRadius will
       take on values of order 10^-2 or so (~10 parsecs); membershipProbability
       has no units but should be within 0 and 1; exponentialVolumeCutoff is
       around 8 kpc, look at Bailer-Jones (2015) for its meaning.
       The output can be minus infinity where the prior is zero, but no
       warnings are returned; this is normal since the prior is just rejecting
       unphysical distances such as negative ones.

    '''

    if clusterDistance <= 0:
        raise Exception('clusterDistance should be positive. This is the distance from the observer to the cluster based on prior knowledge, e.g. literature data.')
    if clusterScaleRadius <= 0:
        raise Exception('clusterScaleRadius should be positive. This is the size of the cluster, e.g. its half-mass radius, from previous knowledge such as literature data.')
    if membershipProbability < 0:
        raise Exception('membershipProbability should be non-negative. This is the probability of membership estimated e.g. with proper motions.')
    if membershipProbability > 1:
        raise Exception('membershipProbability should be less than or equal to 1. This is the probability of membership estimated e.g. with proper motions.')
    if exponentialVolumeCutoff <= 0:
        raise Exception('exponentialVolumeCutoff should be positive. This is the length scale in the exponentially decreasing volume density prior for field stars by Bailer-Jones (2015)')      
    clusterPrior = (1.0/(np.sqrt(2.0*np.pi)*clusterScaleRadius))*np.exp(-((r-clusterDistance)**2.0)/(2.0*clusterScaleRadius*clusterScaleRadius))
    contaminantPrior = (0.5*(np.sign(r) + 1.0)/(2.0*exponentialVolumeCutoff*exponentialVolumeCutoff*exponentialVolumeCutoff))*r*r*np.exp(-r/exponentialVolumeCutoff) 
    pr = membershipProbability * clusterPrior + (1.0 - membershipProbability) * contaminantPrior
    return(np.log(pr))

def loglikelihood(r, parallax, sigmaParallax):
    '''log likelihood probability distribution for stars

       Returns the log of the probability density function (pdf) of the
       likelihood for a given distance r.

       Arguments:
       r -- distance at which to compute the likelihood pdf
       parallax -- parallax measured by Gaia
       sigmaParallax -- Uncertainty on the parallax. Should be > 0.

       Notes:
       The units used for the inputs should be consistent. This means that
       working with r in kpc units the parallax should be measured in units
       such that 1/parallax is in kiloparsec, i.e. in kpc^-1.
       This function is usually called with the argument r being an array of
       distances onto which the likelihood pdf needs to be evaluated; this can
       range e.g. from -1 to 40 (units in kpc).

    '''

    if sigmaParallax <= 0:
        raise Exception('sigmaParallax should be positive. This is the Gaia parallax error.')
    return(-0.5*np.log(2.0*np.pi) - np.log(sigmaParallax) - (((1.0/r) - parallax)**2.0)/(2.0*sigmaParallax*sigmaParallax))

def logposterior(clusterDistance, clusterScaleRadius, membershipProbability, exponentialVolumeCutoff, parallax, sigmaParallax, step, lowerBound, upperBound):
    '''log posterior probability distribution for stars

       Returns the log of the probability density function (pdf) of the
       posterior for a given distance r.

       Arguments:
       clusterDistance -- distance from the observer to the cluster based
           on prior knowledge, e.g. literature data. Should be > 0.
       clusterScaleRadius -- size of the cluster, e.g. its half-mass radius,
           from previous knowledge such as literature data. Should be > 0.
       membershipProbability -- the probability of membership of a star
           estimated independently of parallax, e.g. with proper motions.
           Being a probability, it should be between 0 and 1, inclusive.
       exponentialVolumeCutoff -- length scale in the exponentially decreasing
           volume density prior for field stars by Bailer-Jones (2015).
           Should be > 0.
       parallax -- parallax measured by Gaia
       sigmaParallax -- Uncertainty on the parallax. Should be > 0.
       step -- stepsize of the regular grid of distances onto which the
           posterior pdf is to be computed. Should be > 0.
       lowerBound -- lower bound of the grid 
       upperBound -- upper bound of the grid

       Notes:
       The units used for the inputs should be consistent. This means that
       working with lowerBound, upperBound, and step in kpc units the parallax
       should be measured in units such that 1/parallax is in kiloparsec, i.e.
       in kpc^-1. Similarly the other parameters such as clusterDistance should
       also be in kpc. clusterDistance is typically ~ 1 kpc (as was the case
       for M67) or less; clusterScaleRadius will take on values of order 10^-2
       or so (~10 parsecs); membershipProbability has no units but should be
       within 0 and 1; exponentialVolumeCutoff is around 8 kpc, look at
       Bailer-Jones (2015) for its meaning. The output can be minus infinity
       where the prior is zero, but no warnings are returned; this is normal
       since the prior is just rejecting unphysical distances such as negative
       ones. lowerBound is the distance from which the grid of pdf values is
       computed. It should be 0 or minus epsilon for most uses (the pdf is
       zero anyway for negative values of the distance with the usual prior);
       upperBound is the distance to which to compute the grid, usually it
       should be further away than clusterDistance; step is the distance
       between subsequent points on the grid. Keep it small (e.g. 10^-3 kpc)
       to precisely describe the posterior, but if too small the code is slow.

    '''

    if upperBound <= lowerBound:
        raise Exception('upperBound should be strictly greater than lowerBound. These are the bounds in distance within which to compute the posterior.')
    if step <= 0:
        raise Exception('step should be positive. This is the stepsize of the grid to compute the posterior.')    
    r = np.arange(start = lowerBound, stop = upperBound, step = step) 
    logposteriorArray = np.column_stack((r, loglikelihood(r, parallax, sigmaParallax) + logprior(r, clusterDistance, clusterScaleRadius, membershipProbability, exponentialVolumeCutoff)))
    return(logposteriorArray)

def posteriorMode(clusterDistance, clusterScaleRadius, membershipProbability, exponentialVolumeCutoff, parallax, sigmaParallax, step, lowerBound, upperBound):
    '''modes of the posteriors for an array of membership probabilities,
       parallaxes, and parallax errors
    
       Returns the maximum of the posterior probability density function (pdf).

       Arguments:
       clusterDistance -- distance from the observer to the cluster based
           on prior knowledge, e.g. literature data. Should be > 0.
       clusterScaleRadius -- size of the cluster, e.g. its half-mass radius,
           from previous knowledge such as literature data. Should be > 0.
       membershipProbability -- the probability of membership of a star
           estimated independently of parallax, e.g. with proper motions.
           Being a probability, it should be between 0 and 1, inclusive.
           This is a numpy array (many stars are evaluated at once).
       exponentialVolumeCutoff -- length scale in the exponentially decreasing
           volume density prior for field stars by Bailer-Jones (2015).
           Should be > 0.
       parallax -- parallax measured by Gaia; this is a numpy array.
       sigmaParallax -- Uncertainty on the parallax. Should be > 0. also a
           numpy array.
       step -- stepsize of the regular grid of distances onto which the
           posterior pdf is to be computed. Should be > 0.
       lowerBound -- lower bound of the grid 
       upperBound -- upper bound of the grid

       Notes:
       The units used for the inputs should be consistent. This means that
       working with lowerBound, upperBound, and step in kpc units the parallax
       should be measured in units such that 1/parallax is in kiloparsec, i.e.
       in kpc^-1. Similarly the other parameters such as clusterDistance should
       also be in kpc. clusterDistance is typically ~ 1 kpc (as was the case
       for M67) or less; clusterScaleRadius will take on values of order 10^-2
       or so (~10 parsecs); membershipProbability has no units but should be
       within 0 and 1; exponentialVolumeCutoff is around 8 kpc, look at
       Bailer-Jones (2015) for its meaning.
       membershipProbability, parallax, and sigmaParallax should be numpy
       arrays of the same length. Each entry represents a star.
       The output is also an array of the same length, with the distance of
       each star. Note that in general this may be misleading: the posterior
       may be bimodal, i.e. the code is uncertain whether the star is really
       a member of the cluster or not. Ideally the posterior should not be
       summarized, but used as a whole instead.

    '''

    if len(parallax) != len(sigmaParallax):
        raise Exception('parallax array and sigmaParallax array should be the same length. These are your observational parallax data from Gaia and their associated errors.')
    if len(parallax) != len(membershipProbability):
        raise Exception('parallax array and membershipProbability array should be the same length. These are your observational parallax data from Gaia and the membership probability of the associated star.')
    posteriorModes = np.zeros(len(parallax))
    for i in np.arange(len(parallax)):
        lp = logposterior(clusterDistance, clusterScaleRadius, membershipProbability[i], exponentialVolumeCutoff, parallax[i], sigmaParallax[i], step, lowerBound, upperBound)
        modeIndex = np.argmax(lp[:,1])
        posteriorModes[i] = lp[modeIndex,0]
    return(posteriorModes)



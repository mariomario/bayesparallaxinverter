Bayesparallax module for estimating stellar distances based on parallaxes and membership probabilities.

From a Python instance running in the same folder as bayesparallax.py use
import bayesparallax as bp to load the module. This is demonstrated in the
script prova.py. Get help on each function by typing help(functionName),
e.g. help(bp.posteriorMode). This will provide a detailed explanation of
each function's usage, including input variables, etc.

The only function intended for the end user is posteriorMode.
It takes in input the following:

-       clusterDistance -- distance from the observer to the cluster based
           on prior knowledge, e.g. literature data. Should be > 0.
-       clusterScaleRadius -- size of the cluster, e.g. its half-mass radius,
           from previous knowledge such as literature data. Should be > 0.
-       membershipProbability -- the probability of membership of a star
           estimated independently of parallax, e.g. with proper motions.
           Being a probability, it should be between 0 and 1, inclusive.
           This is a numpy array (many stars are evaluated at once).
-       exponentialVolumeCutoff -- length scale in the exponentially decreasing
           volume density prior for field stars by Bailer-Jones (2015).
           Should be > 0.
-       parallax -- parallax measured by Gaia; this is a numpy array.
-       sigmaParallax -- Uncertainty on the parallax. Should be > 0. also a
           numpy array.
-       step -- stepsize of the regular grid of distances onto which the
           posterior pdf is to be computed. Should be > 0.
-       lowerBound -- lower bound of the grid 
-       upperBound -- upper bound of the grid

Typical usage, e.g. for M67:

- bp.posteriorMode(0.86, 0.05, membershipProbability, 8, parallax, sigmaParallax, 0.001, -1, 30)

where membershipProbability, parallax, and sigmaParallax are numpy arrays of the same length, with each entry representing a star. In this example, distances are in units of kpc so parallax and sigmaParallax should be in kpc^-1, i.e. 1/parallax should be a distance in kpc.

Keep in mind that it is very unlikely that you get reasonable results at the first try, even if the prior looks reasonable. In particular if the prior is too strongly peaked (clusterScaleRadius is too small) the cluster will appear squeezed along the line of sight, vice-versa if it is too large it will appear sigar-shaped. Whether you are allowed to change your prior to get results that appear physically reasonable is quite the philosophical issue. Note that at this stage each star is treated independently, i.e. the information that is learned from it is not used to update the prior. Eventually the code will be changed to do that.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mariomario%2Fbayesparallaxinverter/HEAD?filepath=example.ipynb)


